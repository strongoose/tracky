fmt:
	go fmt ./...
	tidy -quiet -modify --tidy-mark no \
		--indent yes --indent-spaces 2 \
		./internal/static/html/* \
		|| [ "$$?" != 2 ]

test: fmt
	go test ./...

run:
	go run main.go

.phony: test fmt run
