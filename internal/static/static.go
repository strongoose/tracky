package static

import _ "embed"

//go:embed html/index.html
var IndexHTML []byte

//go:embed css/site.css
var SiteCSS []byte
